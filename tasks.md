# Tasks

## Easy tasks, great for first-time contributions:

* Check if apps are available on Arch, Mobian or Flathub.
* If you are running another distribution, feel free to add an extra column for it and start filling it.
* add a new app.

## Content tasks:

* Remove apps that are no longer available (e.g. source code gone) by copying them to archive.csv.
* Branch out games to games.csv, create a games.html page and link that on index.html.

## Design tasks:

* Make the top menu work on mobile.
* Improve mobile design generally.
* Implement a way to display screenshots and or app logos.
